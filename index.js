require('console-stamp')(console, {
	pattern: 'dd/mm/yy HH:MM:ss.l',
	colors: {
		stamp: 'yellow',
		label: 'green'
	}
});

const Discord = require('discord.js');

const config = require('./config.json');

const Client = new Discord.Client();

const deleter = require('./deleter.js');

let Guild = null;

let channels = [];

Client.login(config.token).then(() => {
	console.log('Logged in');
});

Client.on('ready', () => {
	console.log('Ready');
	Guild = Client.guilds.get(config.guild);
	if(!Guild){
		console.error('Guild not joined or invalid ID');
		return shutdown();
	}
	config.channels.forEach(c => {
		c = Guild.channels.get(c);
		switch(c.type){
			case 'text':
				channels.push(c);
				break;
			case 'category':
				channels = channels.concat(Guild.channels.filter(_c => _c.parentID === c.id && _c.type === 'text').array());
				break;
		}
	});
});

Client.on('guildMemberRemove', member => {
	if(member.guild.id !== config.guild) return;
	const logChannel = Guild.channels.get(config.logChannel);
	logChannel.send(`Deleting messages from \`${member.user.username}#${member.user.discriminator}\``);
	deleter.delete(member.user, channels, n => {
		if(config.logChannel)
			logChannel.send(`Deleted ${n} messages from \`${member.user.username}#${member.user.discriminator}\``);
	});
});

Client.on('error', err => console.error(err));

process.setMaxListeners(0);

process.on('uncaughtException', err => console.error(err.stack));

process.on('unhandledRejection', err => console.error(`Uncaught Promise Rejection: \n${err.stack}`));

let shutdown = () => {
	console.log('Disconnecting');
	Client.destroy()
		.then(() => {
			console.log('Disconnected');
			process.exit(0);
		})
		.catch(err => {
			console.error(err);
			process.exit(1);
		})
};

process.on('SIGINT', () => shutdown());